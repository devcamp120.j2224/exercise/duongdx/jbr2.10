public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Duong", "Duong@gmail.com", 'm');
        Author author2 = new Author("Linh", "Linh@gmail.com", 'f');

        System.out.println(author1);
        System.out.println(author2);

        Book book1 = new Book("NguoiSoi", author1, 250000);
        Book book2 = new Book("Iron Man", author2, 300000, 3);

        System.out.println("Thong Tin Nhung Quyen Sach Va tac gia");
        System.out.println(book1);
        System.out.println(book2);
    }
}
